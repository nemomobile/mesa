Name:       mesa

Summary:    Mesa graphics libraries
Version:    19.3.3
Release:    0
Group:      System/Libraries
License:    MIT
URL:        http://www.mesa3d.org/
Source0:    %{name}-%{version}.tar.bz2

BuildRequires:  pkgconfig(libdrm)
BuildRequires:  pkgconfig(libxml-2.0)
BuildRequires:  pkgconfig(talloc)
BuildRequires:  pkgconfig(libudev) >= 160
BuildRequires:  pkgconfig(wayland-client)
BuildRequires:  pkgconfig(wayland-egl)
BuildRequires:  pkgconfig(wayland-protocols)
BuildRequires:  pkgconfig(wayland-server)
BuildRequires:  pkgconfig meson
BuildRequires:  expat-devel >= 2.0
BuildRequires:  python3-devel
BuildRequires:  python3-mako
BuildRequires:  libxml2-python
BuildRequires:  bison
BuildRequires:  flex
BuildRequires:  llvm-devel
BuildRequires:  gettext

%description
Mesa is an open-source implementation of the OpenGL specification  -
a system for rendering interactive 3D graphics.

%package libgbm
Summary:    Generic buffer management API
Group:      System/Libraries
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Provides:   libgbm = %{version}-%{release}

%description libgbm
Generic buffer management API

%package libgbm-devel
Summary:    Mesa libgbm development package
Group:      System/Libraries
Requires:   %{name}-libgbm = %{version}-%{release}
Provides:   libgbm-devel

%description libgbm-devel
Mesa libgbm development package.

%package libglapi
Summary:    Mesa shared gl api library
Group:      System/Libraries
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description libglapi
Mesa shared gl api library.

%package libGLESv1
Summary:    Mesa libGLESv1 runtime libraries
Group:      System/Libraries
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Provides:   libGLESv1 = %{version}-%{release}

%description libGLESv1
Mesa libGLESv1 runtime library.

%package libGLESv2
Summary:    Mesa libGLESv2 runtime libraries
Group:      System/Libraries
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Provides:   libGLESv2 = %{version}-%{release}

%description libGLESv2
Mesa libGLESv2 runtime library.

%package libEGL
Summary:    Mesa libEGL runtime libraries and DRI drivers
Group:      System/Libraries
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig
Provides:   libEGL = %{version}-%{release}

%description libEGL
Mesa libEGL runtime library.


%package libglapi-devel
Summary:    Mesa libglapi development package
Group:      System/Libraries
Requires:   %{name}-libglapi = %{version}-%{release}
Provides:   libglapi-devel

%description libglapi-devel
Mesa libglapi development package.

%package libGLESv1-devel
Summary:    Mesa libGLESv1 development package
Group:      Development/Libraries
Requires:   %{name}-libGLESv1 = %{version}-%{release}
Provides:   libGLESv1-devel

%description libGLESv1-devel
Mesa libGLESv1 development packages

%package libGLESv2-devel
Summary:    Mesa libGLESv2 development package
Group:      Development/Libraries
Requires:   %{name}-libGLESv2 = %{version}-%{release}
Provides:   libGLESv2-devel

%description libGLESv2-devel
Mesa libGLESv2 development packages

%package libEGL-devel
Summary:    Mesa libEGL development package
Group:      Development/Libraries
Requires:   %{name}-libEGL = %{version}-%{release}
Provides:   libEGL-devel

%description libEGL-devel
Mesa libEGL development packages

%package libGL-devel
Summary:    Mesa libGL development package
Group:      Development/Libraries
Requires:   %{name}-libGL = %{version}-%{release}
Provides:   libGL-devel

%description libGL-devel
Mesa libGL development packages

%package dri-drivers-devel
Summary:    Mesa-based DRI development files
Group:      Development/Libraries

%description dri-drivers-devel
Mesa-based DRI driver development files.

%package dri-swrast-driver
Summary:    Mesa-based DRI drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-swrast-driver
Mesa-based swrast DRI driver.

%package dri-lima-driver
Summary:    Mesa-based DRI drivers
Group:      Graphics/Display and Graphics Adaptation
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-lima-driver
Mesa-based lima dri driver.

%package dri-armada-driver
Summary:    Mesa-based armada drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-armada-driver
Mesa-based armada driver.

%package dri-exynos-driver
Summary:    Mesa-based exynos drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-exynos-driver
Mesa-based exynos driver.

%package dri-hx8357d-driver
Summary:    Mesa-based hx8357d drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-hx8357d-driver
Mesa-based hx8357d driver.


%package dri-ili9225-driver
Summary:    Mesa-based ili9225 drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-ili9225-driver
Mesa-based ili9225 driver.

%package dri-ili9341-driver
Summary:    Mesa-based ili9341 drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-ili9341-driver
Mesa-based ili9341 driver.

%package dri-imx-driver
Summary:    Mesa-based imx drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-imx-driver
Mesa-based imx driver.

%package dri-meson-driver
Summary:    Mesa-based meson drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-meson-driver
Mesa-based meson driver.

%package dri-mi0283qt-driver
Summary:    Mesa-based mi0283qt drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-mi0283qt-driver
Mesa-based mi0283qt driver.

%package dri-pl111-driver
Summary:    Mesa-based pl111 drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-pl111-driver
Mesa-based pl111 driver.

%package dri-repaper-driver
Summary:    Mesa-based repaper drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-repaper-driver
Mesa-based repaper driver.

%package dri-rockchip-driver
Summary:    Mesa-based rockchip drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-rockchip-driver
Mesa-based rockchip driver.

%package dri-st7586-driver
Summary:    Mesa-based st7586 drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-st7586-driver
Mesa-based st7586 driver.

%package dri-st7735r-driver
Summary:    Mesa-based st7735r drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-st7735r-driver
Mesa-based st7735r driver.

%package dri-sun4i-driver
Summary:    Mesa-based sun4i drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-sun4i-driver
Mesa-based sun4i driver.

%package dri-mxsfb-driver
Summary:    Mesa-based mxsfb drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-mxsfb-driver
Mesa-based mxsfb driver.

%package dri-stm-driver
Summary:    Mesa-based stm drivers
Group:      Graphics/Display and Graphics Adaptation
Requires:   %{name} = %{version}
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-stm-driver
Mesa-based stm driver.

%package dri-panfrost-driver
Summary:    Mesa-based DRI drivers
Group:      Graphics/Display and Graphics Adaptation
Requires(post): /sbin/ldconfig
Requires(postun): /sbin/ldconfig

%description dri-panfrost-driver
Mesa-based panfrost dri driver.

%prep
%setup -q -n %{name}-%{version}/upstream

%build
%meson -Ddri-drivers= \
    -Dosmesa=none \
    -Ddri3=false \
    -Dgbm=true \
    -Dllvm=true \
    -Dshared-llvm=false \
    -Dgallium-drivers=swrast,lima,panfrost,kmsro \
    -Dvulkan-drivers= \
    -Dplatforms=drm,wayland \
    -Dglx=disabled \
    -Degl=true \
    -Dgles1=true \
    -Dgles2=true

%meson_build

%install
%meson_install

%post libgbm -p /sbin/ldconfig
%postun libgbm -p /sbin/ldconfig

%post libglapi -p /sbin/ldconfig
%postun libglapi -p /sbin/ldconfig

%post libGLESv1 -p /sbin/ldconfig
%postun libGLESv1 -p /sbin/ldconfig

%post libGLESv2 -p /sbin/ldconfig
%postun libGLESv2 -p /sbin/ldconfig

%post libEGL -p /sbin/ldconfig
%postun libEGL -p /sbin/ldconfig

%post dri-swrast-driver -p /sbin/ldconfig
%postun dri-swrast-driver -p /sbin/ldconfig

%post dri-lima-driver -p /sbin/ldconfig
%postun dri-lima-driver -p /sbin/ldconfig

%post dri-armada-driver -p /sbin/ldconfig
%postun dri-armada-driver -p /sbin/ldconfig

%post dri-exynos-driver -p /sbin/ldconfig
%postun dri-exynos-driver -p /sbin/ldconfig

%post dri-hx8357d-driver -p /sbin/ldconfig
%postun dri-hx8357d-driver -p /sbin/ldconfig

%post dri-ili9225-driver -p /sbin/ldconfig
%postun dri-ili9225-driver -p /sbin/ldconfig

%post dri-ili9341-driver -p /sbin/ldconfig
%postun dri-ili9341-driver -p /sbin/ldconfig

%post dri-imx-driver -p /sbin/ldconfig
%postun dri-imx-driver -p /sbin/ldconfig

%post dri-meson-driver -p /sbin/ldconfig
%postun dri-meson-driver -p /sbin/ldconfig

%post dri-mi0283qt-driver -p /sbin/ldconfig
%postun dri-mi0283qt-driver -p /sbin/ldconfig

%post dri-pl111-driver -p /sbin/ldconfig
%postun dri-pl111-driver -p /sbin/ldconfig

%post dri-repaper-driver -p /sbin/ldconfig
%postun dri-repaper-driver -p /sbin/ldconfig

%post dri-rockchip-driver -p /sbin/ldconfig
%postun dri-rockchip-driver -p /sbin/ldconfig

%post dri-st7586-driver -p /sbin/ldconfig
%postun dri-st7586-driver -p /sbin/ldconfig

%post dri-st7735r-driver -p /sbin/ldconfig
%postun dri-st7735r-driver -p /sbin/ldconfig

%post dri-sun4i-driver -p /sbin/ldconfig
%postun dri-sun4i-driver -p /sbin/ldconfig

%post dri-mxsfb-driver -p /sbin/ldconfig
%postun dri-mxsfb-driver -p /sbin/ldconfig

%post dri-stm-driver -p /sbin/ldconfig
%postun dri-stm-driver -p /sbin/ldconfig

%post dri-panfrost-driver -p /sbin/ldconfig
%postun dri-panfrost-driver -p /sbin/ldconfig

%files
%defattr(-,root,root,-)
%dir %{_datadir}/drirc.d
%{_datadir}/drirc.d/00-mesa-defaults.conf

%files libgbm
%defattr(-,root,root,-)
%{_libdir}/libgbm.so.1
%{_libdir}/libgbm.so.1.*

%files libgbm-devel
%defattr(-,root,root,-)
/usr/include/gbm.h
%{_libdir}/libgbm.so
%{_libdir}/pkgconfig/gbm.pc

%files libglapi
%defattr(-,root,root,-)
%{_libdir}/libglapi.so.0
%{_libdir}/libglapi.so.0.*

%files libGLESv1
%defattr(-,root,root,-)
%{_libdir}/libGLESv1_CM.so.*

%files libGLESv2
%defattr(-,root,root,-)
%{_libdir}/libGLESv2.so.*

%files libEGL
%defattr(-,root,root,-)
%{_libdir}/libEGL.so.*

%files libglapi-devel
%defattr(-,root,root,-)
%{_libdir}/libglapi.so
%defattr(-,root,root,-)
%{_includedir}/GL/gl.h
%{_includedir}/GL/glcorearb.h
%{_includedir}/GL/gl_mangle.h
%{_includedir}/GL/glext.h
%dir %{_includedir}/GL/internal
%{_includedir}/GL/internal/dri_interface.h

%files libGLESv1-devel
%defattr(-,root,root,-)
%{_libdir}/libGLESv1_CM.so
%{_includedir}/GLES/egl.h
%{_includedir}/GLES/gl.h
%{_includedir}/GLES/glext.h
%{_includedir}/GLES/glplatform.h
%{_libdir}/pkgconfig/glesv1_cm.pc

%files libGLESv2-devel
%defattr(-,root,root,-)
%{_libdir}/libGLESv2.so
%{_includedir}/GLES2/gl2.h
%{_includedir}/GLES2/gl2ext.h
%{_includedir}/GLES2/gl2platform.h
%{_includedir}/GLES3/gl3.h
%{_includedir}/GLES3/gl31.h
%{_includedir}/GLES3/gl32.h
%{_includedir}/GLES3/gl3ext.h
%{_includedir}/GLES3/gl3platform.h
%{_libdir}/pkgconfig/glesv2.pc

%files libEGL-devel
%defattr(-,root,root,-)
%{_libdir}/libEGL.so
%dir %{_includedir}/EGL
%{_includedir}/EGL/egl.h
%{_includedir}/EGL/eglext.h
%{_includedir}/EGL/eglextchromium.h
%{_includedir}/EGL/eglplatform.h
%{_includedir}/EGL/eglmesaext.h
%dir %{_includedir}/KHR
%{_includedir}/KHR/khrplatform.h
%{_libdir}/pkgconfig/egl.pc

%files libGL-devel
%defattr(-,root,root,-)
%{_includedir}/GL/gl.h
%{_includedir}/GL/glcorearb.h
%{_includedir}/GL/glext.h
%dir %{_includedir}/GL/internal
%{_includedir}/GL/internal/dri_interface.h

%files dri-drivers-devel
%defattr(-,root,root,-)
%{_libdir}/pkgconfig/dri.pc

%files dri-swrast-driver
%defattr(-,root,root,-)
%{_libdir}/dri/swrast_dri.so
%{_libdir}/dri/kms_swrast_dri.so

%files dri-lima-driver
%defattr(-,root,root,-)
%{_libdir}/dri/lima_dri.so

%files dri-armada-driver
%defattr(-,root,root,-)
%{_libdir}/dri/armada-drm_dri.so

%files dri-exynos-driver
%defattr(-,root,root,-)
%{_libdir}/dri/exynos_dri.so

%files dri-hx8357d-driver
%defattr(-,root,root,-)
%{_libdir}/dri/hx8357d_dri.so

%files dri-ili9225-driver
%defattr(-,root,root,-)
%{_libdir}/dri/ili9225_dri.so

%files dri-ili9341-driver
%defattr(-,root,root,-)
%{_libdir}/dri/ili9341_dri.so

%files dri-imx-driver
%defattr(-,root,root,-)
%{_libdir}/dri/imx-drm_dri.so

%files dri-meson-driver
%defattr(-,root,root,-)
%{_libdir}/dri/meson_dri.so

%files dri-mi0283qt-driver
%defattr(-,root,root,-)
%{_libdir}/dri/mi0283qt_dri.so

%files dri-pl111-driver
%defattr(-,root,root,-)
%{_libdir}/dri/pl111_dri.so

%files dri-repaper-driver
%defattr(-,root,root,-)
%{_libdir}/dri/repaper_dri.so

%files dri-rockchip-driver
%defattr(-,root,root,-)
%{_libdir}/dri/rockchip_dri.so

%files dri-st7586-driver
%defattr(-,root,root,-)
%{_libdir}/dri/st7586_dri.so

%files dri-st7735r-driver
%defattr(-,root,root,-)
%{_libdir}/dri/st7735r_dri.so

%files dri-sun4i-driver
%defattr(-,root,root,-)
%{_libdir}/dri/sun4i-drm_dri.so

%files dri-mxsfb-driver
%defattr(-,root,root,-)
%{_libdir}/dri/mxsfb-drm_dri.so

%files dri-stm-driver
%defattr(-,root,root,-)
%{_libdir}/dri/stm_dri.so

%files dri-panfrost-driver
%defattr(-,root,root,-)
%{_libdir}/dri/panfrost_dri.so
